
import pandas as pd
from matplotlib import pyplot as plt

df = pd.read_csv("matches.csv")
dict1 = {}


for i in range(len(df)):
    if df['team1'][i] == 'Rising Pune Supergiant':
        df['team1'][i] = 'Rising Pune Supergiants'
    if df['team2'][i] == 'Rising Pune Supergiant':
        df['team2'][i] = 'Rising Pune Supergiants'
    if df['team1'][i] in dict1:
        if df['season'][i] in dict1[df['team1'][i]]:
            dict1[df['team1'][i]][df['season'][i]] += 1
        else:
            dict1[df['team1'][i]][df['season'][i]] = 1
    else:
        t = True
        for j in range(2008, 2020):
            if t is True:
                dict1[df['team1'][i]] = {}
                t = False
            dict1[df['team1'][i]][j] = 0
        dict1[df['team1'][i]][df['season'][i]] += 1

    if df['team2'][i] in dict1:
        if df['season'][i] in dict1[df['team2'][i]]:
            dict1[df['team2'][i]][df['season'][i]] += 1
        else:
            dict1[df['team2'][i]][df['season'][i]] = 1
    else:
        t = True
        for j in range(2008, 2020):
            if t is True:
                dict1[df['team2'][i]] = {}
                t = False
            dict1[df['team2'][i]][j] = 0
        dict1[df['team2'][i]][df['season'][i]] += 1


seasons = ['Team']

for i in range(2008, 2020):
    seasons.append(i)


w = []
for keys in dict1:
    q = []
    q.append(keys)
    for i in range(2008, 2020):
        q.append(dict1[keys][i])
    w.append(q)


d = pd.DataFrame(w, columns=seasons)
d.plot(x='Team', kind='bar', stacked=True,
       title='Stacked chart of matches played by team by season')

plt.show()
