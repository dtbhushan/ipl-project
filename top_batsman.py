'''pandas to manipulate tabular data and matplotlib to plot charts'''
import pandas as pd
from matplotlib import pyplot as plt

df = pd.read_csv('deliveries.csv')
batsmanAndRuns = {}
for i in range(len(df)):
    if df['batting_team'][i] == 'Royal Challengers Bangalore':
        if df['batsman'][i] in batsmanAndRuns:
            batsmanAndRuns[df['batsman'][i]] += df['batsman_runs'][i]
        else:
            batsmanAndRuns[df['batsman'][i]] = df['batsman_runs'][i]
plt.bar(batsmanAndRuns.keys(), batsmanAndRuns.values())
plt.xticks(
    rotation=90,
    fontsize=7
)
plt.show()
