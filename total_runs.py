'''pandas to manipulate tabular data and matplotlib to plot charts'''
import pandas as pd
from matplotlib import pyplot as plt

df = pd.read_csv('deliveries.csv')
dict1 = {}
for i in range(len(df)):
    if df['batting_team'][i] in dict1:
        dict1[df['batting_team'][i]] += df['total_runs'][i]
    else:
        dict1[df['batting_team'][i]] = df['total_runs'][i]
plt.bar(dict1.keys(), dict1.values())

plt.xticks(
    rotation=30,
    horizontalalignment='right'
)

plt.show()
