'''pandas to manipulate tabular data and matplotlib to plot charts'''
import pandas as pd
from matplotlib import pyplot as plt
df = pd.read_csv('list_of_umpires.csv')
dict1 = {}
for i in range(len(df)):
    if df['Nationality'][i] != 'India':
        if df['Nationality'][i] in dict1:
            dict1[df['Nationality'][i]] += 1
        else:
            dict1[df['Nationality'][i]] = 1

plt.bar(dict1.keys(), dict1.values())
plt.show()
